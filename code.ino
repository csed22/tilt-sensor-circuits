//Inspired by: https://github.com/MertArduino/How-to-use-TILT-sensor-with-LED-BUZZER-and-ARDUINO/blob/master/TiltSensorExample.ino
#define TILT A5
#define BUZZER 3
#define LED 12

void setup()
{
	pinMode(TILT, INPUT);
	pinMode(LED, OUTPUT);
	pinMode(BUZZER, OUTPUT);
}

void loop()
{
	// Tilt Sensor is on when the TILT pin is LOW
	int digitalTilt = digitalRead(TILT);
	digitalWrite(LED, !digitalTilt);

	int analogTilt = analogRead(TILT);
	// analogRead scales from 0 to 1023
	// analogWrite scales from 0 to 255
	int buzzerOutput = 255 - (analogTilt / 4);
	analogWrite(BUZZER, buzzerOutput);
}